import { Component } from '@angular/core';
import { DataService } from '../data.service';
@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})

export class CountryListComponent {
  data: any[]=[];
  sortedList: any[]=[];
  filteredCountries:any[]=[];
  currentPage = 1;
  itemsPerPage = 20;
  sortDirection: 'asc' | 'desc' = 'asc';
  constructor(private dataService: DataService) {}

  ngOnInit() {
    const url = 'https://restcountries.com/v2/all?fields=name,region,area'; 
    this.dataService.getData(url).subscribe(
      (response) => {
        this.data = response;
        this.applyFilters();
      },
      (error) => {
        console.error(error);
      }
    );
  }
  applyFilters() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    let sortedCountries = [...this.data];
    
    sortedCountries.sort((a, b) => {
      if (a.name < b.name) {
        return this.sortDirection === 'asc' ? -1 : 1;
      }
      if (a.name > b.name) {
        return this.sortDirection === 'asc' ? 1 : -1;
      }
      return 0;
    });

    const isSmallerThanLithuania = (country: { area: number; }) => country.area < this.getLithuaniaArea();
    const isInOceaniaRegion = (country: { region: string; }) => country.region === 'Oceania';
    
    this.filteredCountries = sortedCountries.filter((country) => {
      return isSmallerThanLithuania(country) || isInOceaniaRegion(country);
    }).slice(startIndex, endIndex);
  }

  getLithuaniaArea(): number {
    const lithuania = this.data.find((country) => country.name === 'Lithuania');
    return lithuania ? lithuania.area : 0;
  }

  onPageChange(page: number) {
    this.currentPage = page;
    this.applyFilters();
  }

  toggleSortDirection() {
    this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    this.applyFilters();
  }
}
